from flask_restplus import Namespace, Resource, fields
from werkzeug.exceptions import abort

from app.models import Document, db
from app.utils import parse_addreses, parse_dates, parse_money_values, parse_names

api = Namespace('documents', description="Section for documents loading and parsing")

doc = api.model('Document', {
    'id': fields.Integer(read_only=True, description='The document\'s identifier'),
    'text': fields.String(required=True, description='The document\'s main text'),
    'info': fields.String(read_only=True, description='The document\'s parsed elements'),
    })

doccreate = api.model('DocumenCreate', {
    'text': fields.String(required=True, description='The document\'s main text'),
    })


@api.route('/')
class DocumentCollection(Resource):

    @api.marshal_list_with(doc)
    def get(self):
        return Document.query.all()

    # TODO: переделать загрузку и парсинг текста в виде celery task и отдавать только ссылку на очередь, а не сам объект
    @api.expect(doccreate)
    @api.marshal_with(doc, code=201)
    def post(self):
        text = api.payload['text']
        info = {
            **parse_names(text),
            **parse_dates(text),
            **parse_money_values(text),
            **parse_addreses(text)
            }
        item = Document(text=text, info=str(info))
        db.session.add(item)
        db.session.commit()
        db.session.refresh(item)
        return item


@api.route('/<int:id>')
@api.response(404, 'Document not found')
class DocumentResourse(Resource):

    @api.marshal_with(doc)
    def get(self, id):
        res = Document.query.get(id)
        if res:
            return res
        else:
            abort(404, 'Document not found')
