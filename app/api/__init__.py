from flask_restplus import Api
from .documents import api as documents_api


api = Api(
    title='LegalParser',
    version='1.0',
    description='RESTful api for analyzer of legal documents'
)

api.add_namespace(documents_api, path='/documents')