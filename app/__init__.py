from flask import Flask

from config import get_config_by_name
from .api import api
from .models import db, Document


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(get_config_by_name[config_name])

    db.init_app(app)
    db.create_all(app=app)

    api.init_app(app)

    return app
