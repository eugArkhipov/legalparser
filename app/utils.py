"""
Базовые посылки:
- ФИО мы ищем только в именительном падеже. В большинстве официальных документов как минимум единожды имя контрагента
указывается в именительном падеже(если не в основном теле договора, то в реквизитах)
- Порядок ФИО только такой Ф-И-О. Такой порядок И-О-Ф считаем невалидным, так как в официальных документах используется
только первый

DICSCLAIMER: в данном скрипте для определения ФИО и топонимов используется библиотека pymorphy. Я не уверен, что эта
библиотека достаточно легко может быть масштабирована, и безусловно, использование elasticsearch более приемлемо для прода
Однако, базовый алгоритм поиска на эластике скорее всего будет такой же. Топонимы и имена будут определяться по словарю,
фамилии - скорее всего по расположению к именам и по окончаниям. Да, в данной программе не используется никаких проверок на
Fuzziness. Имена определять итак сложно, а это может еще сильнее снизить качество определения. Кроме того, вполне валидной
является посылка, что в юридических значимых документах почти не допускается грубых опечаток в именах и топонимах, так как
от этого зависит их юридическая сила.

С помощью анализатора текстов мы ищем вхождения следующих строк:
- ФИО: фамилия, имя, отчество
    ПРИМЕЧАНИЕ: алгоритм имеет проблемы с определением не-славянских имен и отчеств. Однако, это решается добавлением
    дополнительных слов в словари(к сожалению, для реализации этого нужно больше времени) Однако восточные отчества, зака-
    нчивающиеся на -кызы и -оглы скрипт определяет(определял бы, будь в словаре восточные имена, ибо Иван кызы - это
    дичь)
- Дата: в разных форматах
- Место: область, город, улица
- Сумма: опять-таки в разных форматах
"""
import nltk
import pymorphy2
import time
import re


def parse_names(document: str):
    # делим документ на слова
    text = nltk.word_tokenize(document)
    analyzer = pymorphy2.MorphAnalyzer()

    def is_name(word):
        # мы выбираем 0 индекс, т.к. pymorphy изначально возвращает их отсортированными по score, т.е. индект 0 - обозначает
        # значение с максимальным значением score
        name = analyzer.parse(word)[0]
        # ищем совпадения по словарю имен и в именительном падеже. + score вхождения достаточно высок
        if ('Name' in name.tag and 'nomn' in name.tag) and name.score >= 0.7:
            return True

        return False

    def is_patronymic(word):
        patronymic = analyzer.parse(word)[0]
        if ('Patr' in patronymic.tag and 'nomn' in patronymic.tag) and patronymic.score >= 0.7:
            return True
        return False

    def is_surname(word):
        variants = analyzer.parse(word)
        # здесь падеж неважен, т.к. можно предположить, что если имя и отчество в именительном падеже, то и фамилия будет в
        # нем же.
        for item in variants:
            if 'Surn' in item.tag:
                return True
        return False

    result = []

    for idx, word in enumerate(text):
        name = word
        if is_name(name):
            patronymic = text[idx + 1]

            if is_patronymic(patronymic):
                surname = text[idx - 1]
                if is_surname(surname):
                    result.append(f'{surname} {name} {patronymic}')

            elif is_name(patronymic) and text[idx + 2].lower() in ['кызы', 'оглы']:
                patronymic += (' ' + text[idx + 2])

                surname = text[idx - 1]
                if is_surname(surname):
                    result.append(f'{surname} {name} {patronymic}')

    # возвращаем набор уникальных значений, т.к. одно и то же значение может повторяться в документе несколько раз
    return {
        'имена': list(set(result))}


def parse_dates(document: str):
    # парсит даты в след.форматах - 21.01.2012, 21 01 2012, "21" января 2012, "21" января 2012 года
    result = re.findall(
        r'(["«]?(3[01]|[12][0-9]|0[1-9])["»]?( +|\.|/)(1[0-2]|0[1-9]|января|февраля|марта|апреля|мая|июня|июля|августа|сентября|октября|ноября|декабря)( +|\.|/)[0-9]{4}( +(г\.|года))?)',
        document)
    # return only main group
    return {
        'даты': list(set([i[0] for i in result]))}


def parse_money_values(document: str):
    money_pattern = re.compile(r'((?:[0-9 ,]+)(р\.|рублей|рубля))')
    result = money_pattern.findall(document)
    return {
        'денежные суммы': list(set([i[0] for i in result]))}


def parse_addreses(document: str):
    """
    Особенности алгоритма поиска адреса заключаются в следующем. Для простоты мы делим адрес на несколько логических блоков:
    индекс/область/населенный пункт/улица/дом/корпус-строение/квартира. Не все блоки обязаны быть, есть три обязтальных блока,
    которые должны присутствовать всегда - город, улица, дом. Также, базовая послыка, что все адреса пишутся только в таком
    порядке а не наоборот. Для юридических документов она верна почти всегда. Соответственно, при поиске адреса наша задача сво-
    дится к поиску этих блоков.

    Так как в валидном адресе обязательно будет минимум три блока, то можно для поиска этих блоков использовать "якоря",
    специальные слова, которые зачастую свзываются в тексте с адресами. Сочетание сразу трех якорей из разных блоков довольно
    маловероятно в обычном тексте, а потому с высокой долей вероятности будет обозначать как раз-таки какой-то адрес

    DISKLAIMER: эта функция самая слабая из всех, но и задача наиболее сложная. Очень много возможных вариантов, и проработать
    каждый - не самая простая задача. Однако, даже в таком варианте некоторые адреса находить уже можно, и в целом этот подход
    вполне можно развить и масштабирова, кмк, и он к тому же не завязан на словари, что делает его довольно нетребовательным к
    ресурсам
    """
    text = document.split()

    district_pattern = r'(область|обл\.|республика|респ\.|край|округ|окр\.|город|гор\.|г\.)'
    street_pattern = r'(город|гор\.|г\.|ул\.|улица|пер\.|переулок|бул\.|бульвар|шоссе|пр\.|пр-кт\.|проспект|проезд)'
    house_pattern = r'(дом|д\.|[0-9]{1,4})'

    result = []

    # TODO: переписать в виде рекурсии
    # TODO: в случае получения совпадения проматывать цикл до места конечного индекса, чтобы исключить дублирование
    for idx, word in enumerate(text):
        # TODO: добавить проверку слова, стоящего перед названием района на то, является ли оно прилагательным(Красноярский край и проч...)
        district = re.findall(district_pattern, word)
        if len(district) != 0:
            # в качестве области поиска следующего якоря берем максимальное расстояние в 4 слова от предыдущего якоря
            for street_i in range(4):
                street = re.findall(street_pattern, text[idx + (street_i + 1)])
                if len(street) != 0:
                    # в качестве области поиска следующего якоря берем максимальное расстояние в 4 слова от предыдущего якоря
                    for house_i in range(4):
                        house = re.findall(house_pattern, text[idx + (street_i + 1) + (house_i + 1)])
                        if len(house) != 0:
                            # TODO: добавить поиск совпадения у рядом стоящего слова с числом в 4-5 разрядов, т.к. это может быть номер дома
                            r = ' '.join(text[idx: (idx + (street_i + 1) + (house_i + 1)) + 1])
                            result.append(r)
                            break
                    break

    return {
        'адреса': list(set(result))}


text = """
Генеральному директору АО «Ромашка»
Сидорову Ивану Михайловичу

от Махнева Евгения Олеговича

Согласие на обработку персональных данных
Настоящим во исполнение требований Федерального закона «О
персональных данных» № 152-ФЗ от 27.07.2006 я,
Махнев Евгений Олегович,
паспорт 11111 выдан ТП №3 межрайонного ОУФМС по городу Москве
"19" августа 2012 г.,
адрес регистрации: респ. Хакасия, г. Абакан, ул.Щетинкина д.65
даю свое письменное согласие Aкционерному Oбществу «Ромашка»
(АО «Ромашка»), место нахождения: 123001, город Москва,
Трехпрудный переулок, д. 9 стр. 1Б, на обработку моих персональных
данных в целях обеспечения соблюдения трудового и гражданского
законодательства и иных нормативных правовых актов, содействия в
трудоустройстве, проведения тестирования или анализа личности,
оказания мне консультационных услуг. Настоящее согласие выдано
сроком на 75 лет.
Я уведомлен и понимаю, что под обработкой персональных данных
подразумевается сбор, систематизация, накопление, хранение,
уточнение (обновление, изменение), использование, распространение
(в том числе передача и трансграничная передача), обезличивание,
блокирование, уничтожение и любые другие действия (операции) с
персональными данными.
Вышеуказанные операции могут осуществляться как с использованием
средств автоматизации, так и без использования таких средств.
Также под персональными данными подразумевается любая
информация, имеющая ко мне отношение как к субъекту персональных
данных, в том числе моя фамилия, имя, отчество, дата и место
рождения, адрес проживания, номер телефона, идентификационный
номер налогоплательщика, номер страхового свидетельства
государственного пенсионного страхования, сведения, включенные в
трудовую книжку, сведения о воинском учете, фотография, семейный
статус, информация о наличии имущества, образование, повышение
квалификации, знание иностранного языка, доходы, гражданство,
профессия, общий трудовой стаж, результаты тестирования и анализа
личности.
Порядок отзыва согласия на обработку персональных данных мне
известен.
Штраф в случае нарушения условий договора составляет - 5 000 рублей
"""

if __name__ == '__main__':
    print(parse_names(text))
    print(parse_dates(text))
    print(parse_money_values(text))
    print(parse_addreses(text))
