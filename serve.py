# -*- coding: utf-8 -*-
from app import create_app
import uuid
from app.models import db, Document

# ------ START SERVER ------
if __name__ == '__main__':
    app = create_app('dev')
    app.run(port=5001)
